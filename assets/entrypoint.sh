#!/bin/bash

echo "CLUSTER_DEPLOYMENT environment variable is set to" $CLUSTER_DEPLOYMENT
echo "DATABASE_SHARED_REGISTRY environment variable is set to" $DATABASE_SHARED_REGISTRY
echo "CLUSTER_MEMBER_ROLE environment variable is set to" $CLUSTER_MEMBER_ROLE

CARBON_OPS="-DportOffset=$CARBON_OFFSET"

echo "Setting the configurations files to properly endpoint generation"
    
if [ $CLUSTER_DEPLOYMENT = "true" ]; then
    echo "Setting the clustering configurations files..."
        
    local_ip=$(ip add | grep global | awk 'NR==1 {print $2}' | cut -d "/" -f 1)
    members="<member><hostName>${local_ip%.*}.1-10</hostName><port>4100</port></member>"
        
    mv /root/clustered-axis2.xml $CARBON_HOME/repository/conf/axis2/axis2.xml
    mv /root/clustered-carbon.xml $CARBON_HOME/repository/conf/carbon.xml
        
    wget http://product-dist.wso2.com/tools/svnkit-all-1.8.7.wso2v1.jar -P $CARBON_HOME/repository/components/dropins
    wget http://maven.wso2.org/nexus/content/groups/wso2-public/com/trilead/trilead-ssh2/1.0.0-build215/trilead-ssh2-1.0.0-build215.jar -P $CARBON_HOME/repository/components/lib
        
    sed -i -e 's#<clustering class="org.wso2.carbon.core.clustering.hazelcast.HazelcastClusteringAgent" enable="false">#<clustering class="org.wso2.carbon.core.clustering.hazelcast.HazelcastClusteringAgent" enable="true">#g' $CARBON_HOME/repository/conf/axis2/axis2.xml
    sed -i -e 's#<parameter name="domain">wso2.carbon.domain</parameter>#<parameter name="domain">'$CLUSTER_DOMAIN'</parameter>#g' $CARBON_HOME/repository/conf/axis2/axis2.xml
    sed -i -e 's#<parameter name="localMemberHost">127.0.0.1</parameter>#<parameter name="localMemberHost">'$local_ip'</parameter>#g' $CARBON_HOME/repository/conf/axis2/axis2.xml
    sed -i -e 's#CLUSTER_MEMBERS#'$members'#g' $CARBON_HOME/repository/conf/axis2/axis2.xml
    sed -i -e 's#<Enabled>SVNBASEDSYNC</Enabled>#<Enabled>true</Enabled>#g' $CARBON_HOME/repository/conf/carbon.xml
    sed -i -e 's#<SvnUrl>http://svnrepo.example.com/repos/</SvnUrl>#<SvnUrl>'$CLUSTER_SVN_URL'</SvnUrl>#g' $CARBON_HOME/repository/conf/carbon.xml
    sed -i -e 's#<SvnUser>username</SvnUser>#<SvnUser>'$CLUSTER_SVN_USER'</SvnUser>#g' $CARBON_HOME/repository/conf/carbon.xml
    sed -i -e 's#<SvnPassword>password</SvnPassword>#<SvnPassword>'$CLUSTER_SVN_PASS'</SvnPassword>#g' $CARBON_HOME/repository/conf/carbon.xml
        
    if [ $CLUSTER_MEMBER_ROLE = "manager" ]; then
        echo "Setting node as Manager of domain $CLUSTER_DOMAIN..."
            
        sed -i -e 's#<property name="port.mapping.8243" value="9444"/>#<property name="port.mapping.8243" value="9444"/-->#g' $CARBON_HOME/repository/conf/axis2/axis2.xml
        sed -i -e 's#<property name="subDomain" value="mgt"/-->#<property name="subDomain" value="mgt"/>#g' $CARBON_HOME/repository/conf/axis2/axis2.xml
        sed -i -e 's#<AutoCommit>false</AutoCommit>#<AutoCommit>true</AutoCommit>#g' $CARBON_HOME/repository/conf/carbon.xml
            
        #CARBON_OPS="$CARBON_OPS -Dsetup"
    else
        echo "Setting node as Worker of domain $CLUSTER_DOMAIN"
            
        sed -i -e 's#<!--property name="subDomain" value="worker"/-->#<property name="subDomain" value="worker"/>#g' $CARBON_HOME/repository/conf/axis2/axis2.xml
            
        #CARBON_OPS="$CARBON_OPS -DworkerSNode=true"
    fi
else
    echo "Setting server to work in standalone mode..."
fi
    
if [ "$DATABASE_SHARED_REGISTRY" = true ]; then
    echo "Setting the database tier..."
    echo "Setting MySQL..."
        
    wget http://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-java-5.1.38.zip -P /root
    unzip /root/mysql-connector-java-5.1.38.zip mysql-connector-java-5.1.38/mysql-connector-java-5.1.38-bin.jar -d /root/
    mv /root/mysql-connector-java-5.1.38/mysql-connector-java-5.1.38-bin.jar $CARBON_HOME/repository/components/lib
        
    sed -i -e 's#<url>jdbc:h2:repository/database/WSO2CARBON_DB;DB_CLOSE_ON_EXIT=FALSE;LOCK_TIMEOUT=60000</url>#<url>'$DATABASE_URL'</url>#g' $CARBON_HOME/repository/conf/datasources/master-datasources.xml
    sed -i -e 's#<username>wso2carbon</username>#<username>'$DATABASE_USER'</username>#g' $CARBON_HOME/repository/conf/datasources/master-datasources.xml
    sed -i -e 's#<password>wso2carbon</password>#<password>'$DATABASE_PASSWD'</password>#g' $CARBON_HOME/repository/conf/datasources/master-datasources.xml
    sed -i -e 's#org.h2.Driver#com.mysql.jdbc.Driver#g' $CARBON_HOME/repository/conf/datasources/master-datasources.xml
        
    sed -i -e "1s/^/SET SQL_MODE='ALLOW_INVALID_DATES';\n\n/" $CARBON_HOME/dbscripts/mysql.sql

    rm -rf /root/mysql-connector-java-5.1.38*
fi
    
sed -i -e 's#<!--HostName>www.wso2.org</HostName-->#<HostName>'$CARBON_HOST'</HostName>#g' $CARBON_HOME/repository/conf/carbon.xml
sed -i -e 's#<!--MgtHostName>mgt.wso2.org</MgtHostName-->#<MgtHostName>'$CARBON_MGT_HOST'</MgtHostName>#g' $CARBON_HOME/repository/conf/carbon.xml
sed -i -e '0,/<!--parameter name="bind-address" locked="false">hostname or IP address<\/parameter-->/s##<parameter name="bind-address" locked="false">http://'$CARBON_HOST':'$((8280 + $CARBON_OFFSET))'</parameter>#g' $CARBON_HOME/repository/conf/axis2/axis2.xml
sed -i -e '0,/<!--parameter name="bind-address" locked="false">hostname or IP address<\/parameter-->/s##<parameter name="bind-address" locked="false">https://'$CARBON_HOST':'$((8243 + $CARBON_OFFSET))'</parameter>#g' $CARBON_HOME/repository/conf/axis2/axis2.xml
sed -i -e 's#<!--parameter name="WSDLEPRPrefix" locked="false">https://apachehost:port/somepath</parameter-->#<parameter name="WSDLEPRPrefix" locked="false">http://'$CARBON_HOST':'$((8280 + $CARBON_OFFSET))'</parameter>#g' $CARBON_HOME/repository/conf/axis2/axis2.xml

rm -rf /root/wso2esb-4.9.0.zip

sed -i -e 's#CARBON_OPS#'$CARBON_OPS'#g' /root/entrypoint.configured.sh
mv /root/entrypoint.configured.sh /root/entrypoint.sh

$CARBON_HOME/bin/wso2server.sh $CARBON_OPS