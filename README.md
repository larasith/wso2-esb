# Usage #

## Standalone mode ##

Deploying a ESB instance is very simple:

```
#!docker

docker run --net=host larasith/wso2-esb
```

The command above deploys an instance of WSO2 ESB using its default H2 in memory database. If you prefer to use a physic one, you could use this command:

```
#!docker

docker run --net=host \
-e DATABASE_SHARED_REGISTRY=true \
-e DATABASE_URL="jdbc:mysql://mysql-host:3306/regdb" \
-e DATABASE_USER=user \
-e DATABASE_PASSWD=passwd \
larasith/wso2-esb
```

*For now, **MySQL** is the only database supported.*

## Cluster mode ##

This image, also allow you to deploy a ESB cluster. You only will have to create a manager container and a workers containers:

### Manager ###
```
#!docker

docker run --name=manager \
-e CLUSTER_DEPLOYMENT=true \
-e CLUSTER_MEMBER_ROLE=manager \
-e CLUSTER_SVN_URL=svn://svn-host:3690/repos \
-e CLUSTER_SVN_USER=svnuser \
-e CLUSTER_SVN_PASSWD=svnpasswd \
-e DATABASE_SHARED_REGISTRY=true \
-e DATABASE_URL="jdbc:mysql://mysql-host:3306/regdb" \
-e DATABASE_USER=user \
-e DATABASE_PASSWD=passwd \
larasith/wso2-esb
```

### Worker ###
```
#!docker

docker run --name=worker \
-e CARBON_OFFSET=1 \
-e CLUSTER_DEPLOYMENT=true \
-e CLUSTER_MEMBER_ROLE=worker \
-e CLUSTER_SVN_URL=svn://svn-host:3690/repos \
-e CLUSTER_SVN_USER=svnuser \
-e CLUSTER_SVN_PASSWD=svnpasswd \
-e DATABASE_SHARED_REGISTRY=true \
-e DATABASE_URL="jdbc:mysql://mysql-host:3306/regdb" \
-e DATABASE_USER=user \
-e DATABASE_PASSWD=passwd \
larasith/wso2-esb
```

Thus you can create a cluster with every worker that you need.

# Environment Variables #

All environment variables are optionals, but most of then should be set to configure the servers properly.

### ``` CARBON_HOST ``` ###
Specifies the server hostname, its default value is **localhost**. This variable is important to ESB can create properly the endpoints to the services exposed.

### ``` CARBON_MGT_HOST ``` ###
Specifies the management server hostname, its default value is the same than previous variable.

### ``` CARBON_OFFSET ``` ###
Specifies the port offset, its default value is **0**. This variable sums its value to the following ports:
* HTTP servlet transport port = 9763
* HTTPS servlet transport port = 9443
* NIO/PT transport port (nhttp) = 8280
* NIO/PT transport port (nhttps) = 8243

### ``` CLUSTER_DEPLOYMENT ``` ###
Specifies whether the deployment is standalone (false) or clustered (true), its default value is **false**.

### ``` CLUSTER_DOMAIN ``` ###
Specifies the domain name, its default value is **wso2.carbon.domain**.

### ``` CLUSTER_MEMBER_ROLE  ``` ###
Specifies the role of the server, its default value is **standalone**. This variable isn't evaluated in standalone deployments. It accepts the following values:

* **standalone**: the container won't join to the cluster. In a cluster mode only **manager** or **worker** values are valid.
* **manager**: indicate that this container has to be configured as a manager of the cluster.
* **worker**: indicate that this container has to be configured as a worker of the cluster.

### ``` CLUSTER_SVN_URL ``` ###
Specifies the Subversion URL, its default value is **svn://$CARBON_HOST:3690/svn/depsyncrepo**. The cluster mode use a SVN-based synchronization, so is mandatory have a Subversion server running.

### ``` CLUSTER_SVN_USER ``` ###
Specifies the Subversion User, its default value is **wso2svnuser**. See the environment variable **CLUSTER_SVN_URL**.

### ``` CLUSTER_SVN_PASS ``` ###
Specifies the Subversion Password, its default value is **passwd**. See the environment variable **CLUSTER_SVN_URL**.

### ``` DATABASE_SHARED_REGISTRY ``` ###
Specifies if is necessary to configure a external database, its default value is **false**. In cluster mode it should be **true** to a correct synchronization between the cluster members.

### ``` DATABASE_URL ``` ###
Specifies the database URL, its default value is **jdbc:mysql://$CARBON_HOST:3306/regdb?autoReconnect=true&amp;useSSL=false**. For the moment, only **MySQL** databases are supported.

### ``` DATABASE_USER ``` ###
Specifies the database User, its default value is **wso2dbuser**. See the environment variable **DATABASE_URL**.

### ``` DATABASE_PASSWD ``` ###
Specifies the database Password, its default value is **passwd**. See the environment variable **DATABASE_URL**.